@extends('layouts.app')

@section('content')

<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- Update Subject Form -->
    <form action="{{ url( 'subjects/'. $subject->id) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <!-- Subject Name -->
        <div class="form-group">
            <label for="{{ $subject->id }}-name" class="col-sm-3 control-label">
                Subject
            </label>

            <div class="col-sm-6">
                <input type="text" name="name" id="{{ $subject->id }}-name" class="form-control" value="{{ $subject->name }}">
            </div>
            
        </div>

        <!-- Update Subject Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Update
                </button>
            </div>
        </div>
    </form>
</div>

@endsection

