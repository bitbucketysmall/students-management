@extends('layouts.app')

@section('content')

<!-- Bootstrap Boilerplate... -->
@can('store', \App\Models\Subject::class)
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- New Subject Form -->
    <form action="{{ url('subjects') }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        <!-- Subject Name -->
        <div class="form-group">
            <label for="subject-name" class="col-sm-3 control-label">
                 subject
            </label>

            <div class="col-sm-6">
                <input type="text" name="name" id="subject-name" class="form-control">
            </div>
            
        </div>

        <!-- Add Subject Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Add subject
                </button>
            </div>
        </div>
    </form>
</div>
@endcan
<!--  Current Subjects -->
<!-- Current Subjects -->
@if (count($subjects) > 0)
<div class="panel panel-default">
    <div class="panel-heading">
        Current subjects
    </div>

    <div class="panel-body">
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
            <th>subject</th>
            </thead>

            <!-- Table Body -->
            <tbody>
                @foreach ($subjects as $subject)
                <tr>
                    <!-- Subject Name -->
                    <td class="table-text">
                        <div>{{ $subject->name }}</div>                     
                    </td>

                   
                    @can('destroy', $subject)
                    <td>
                        <!--  Delete Button -->
                        <form action="{{ url('subjects/'. $subject->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" id="delete-subject-{{ $subject->id }}" class="btn btn-danger">
                                <i class="fa fa-btn fa-trash"></i>Delete
                            </button>
                        </form>
                    </td>
                    @endcan
                    @can('update', $subject)
                    <td>
                        <!--  Update Button -->
                        <form id="update{{ $subject->name }}" action="{{ url('subjects/'. $subject->id) }}" method="GET">
                            {{ csrf_field() }}                         
                            <button type="submit" id="update-subject-{{ $subject->id }}" class="btn btn-info">
                                <i class="fa fa-btn fa-trash"></i>Update
                            </button>
                        </form>
                    </td>
                    @endcan
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>
@endif
@endsection

