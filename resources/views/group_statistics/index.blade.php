@extends('layouts.app')

@section('content')

@can('store', \App\Models\Group::class)
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- New group Form -->
    <form action="groups" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        <!-- group Name -->
        <div class="form-group">
            <label for="group-name" class="col-sm-3 control-label">
                group
            </label>

            <div class="col-sm-6">
                <input type="text" name="name" id="group-name" class="form-control">
            </div>
        </div>

        <!-- Add Task Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Add group
                </button>
            </div>
        </div>
    </form>
</div>
@endcan

<!-- Current groups -->
@if (count($groups) > 0)
<div class="panel panel-default">
    <div class="panel-heading">
        Current groups
    </div>

    <div class="panel-body">
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
            <th>group</th>
            <th>average</th>
            @foreach ($subjects as $subject)
            <th>average {{ $subject->name }}</th>
            @endforeach

            </thead>

            <!-- Table Body -->
            <tbody>
                @foreach ($groups as $group)
                <tr>
                    <!-- group Name -->
                    <td class="table-text">
                        <div>{{ $group->name }}</div>
                    </td>
                    <!-- group average -->
                    <td class="table-text">
                        <div>{{ $group->average }}</div>
                    </td>
                    <!-- group average in subjects -->
                    @foreach ($group->averageSubjects as $avSub)

                    <td>
                        <div>{{ $avSub }}</div>
                    </td>

                    @endforeach
                    @can('destroy', $group)
                    <td>
                        <!--  Delete Button -->
                        <form action="{{ url('groups/'. $group->id) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" id="delete-group-{{ $group->id }}" class="btn btn-danger">
                                <i class="fa fa-btn fa-trash"></i>Delete
                            </button>
                        </form>
                    </td>
                    @endcan
                    @can('update', $group)
                    <td>
                        <!--  Update Button -->
                        <form id="update{{ $group->name }}" action="{{url('groups/'. $group->id) }}" method="GET">
                            {{ csrf_field() }}
                            <button type="submit" id="update-group-{{ $group->id }}" class="btn btn-info">
                                <i class="fa fa-btn fa-trash"></i>Update
                            </button>
                        </form>
                    </td>
                    @endcan
                    <td>
                        <!--  Select link -->


                        <form id="update{{ $group->name }}" action="{{url('groups/'. $group->id. '/students') }}" method="GET">
                            {{ csrf_field() }}

                            <button type="submit" id="students-group-{{ $group->id }}" class="btn btn-light">
                                <i class="fa fa-btn fa-trash"></i>See students
                            </button>
                        </form>
                    </td>

                </tr>

                @endforeach

            </tbody>
        </table>
    </div>
</div>
@endif

@endsection