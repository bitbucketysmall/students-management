@extends('layouts.app')
@section('content')
<!-- Bootstrap Boilerplate... -->
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- Update Subject Form -->
    <form action="{{ url( 'groups/'. $group->id) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}
        {{ method_field('PUT') }}

        <!-- Group Name -->
        <div class="form-group">
            <label for="{{ $group->id }}-name" class="col-sm-3 control-label">
                Group
            </label>

            <div class="col-sm-6">
                <input type="text" name="name" id="{{ $group->id }}-name" class="form-control" value="{{ $group->name }}">
            </div>
            
        </div>

        <!-- Update Group Button -->
        <div class="form-group">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Update
                </button>
            </div>
        </div>
    </form>
</div>
@endsection

