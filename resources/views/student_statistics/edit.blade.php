@extends('layouts.app')

@section('content')
<div class="panel panel-default">      
        <form class="form-horizontal"
              action="{{url('groups/'.$currentStudent->group->id.'/students/'. $currentStudent->id ) }}" 
              enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            @method('PUT')
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="student-name" class="col-sm-3 control-label">
                        name
                    </label>
                    <input value="{{ $currentStudent->name }}" type="text" name="name" id="student-name" class="form-control">

                    <label for="student-birth" class="col-sm-3 control-label">
                        birth
                    </label>
                    <input value="{{ $currentStudent->birth }}" type="date" name="birth" id="student-birth" class="form-control">
                    <label for="student-group" class="col-sm-3 control-label">
                        group
                    </label>
                    <select id="student-group" name="group_id" class="form-control">
                        <option value="{{$currentStudent->group->id}}">{{ $currentStudent->group->name }}</option>
                        @foreach ($groups as  $group)
                        <option value="{{$group->id}}">{{$group->name}}</option>
                        @endforeach
                    </select><br>
                    <input type="submit" value="Обновить" class="btn btn-info">
                </div>
            </div>
        </form>
        <form class="form-horizontal" action="{{url('groups/'.$currentStudent->group->id.'/students/'. $currentStudent->id .'/uploadPhoto' ) }}" enctype="multipart/form-data" method="POST">
            {{ csrf_field() }}
            <div class="form-group">
                <div class="col-sm-6">
                    <label for="student-phto" class="col-sm-3 control-label">
                        Upload a photo:
                    </label>
                    <input  value="photo" type="file" name="image" multiple accept="image/*">
                    <input type="submit" value="Загрузить фото" class="btn btn-info">
                </div>
            </div>
        </form>
<a style="margin-left: 15px;" class="btn btn-primary" href="{{ route('groups.students.show', [ $currentStudent->group, $currentStudent]) }}">Назад</a>
</div>
@endsection

