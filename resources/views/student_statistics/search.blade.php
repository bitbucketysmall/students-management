@extends('layouts.app')
@section('content')

<!-- Current students -->

<div class="panel panel-default">
    <div class="panel-heading">
        SEARCH PREFERENCIES 
    </div>
    <br>
    <div class="panel-body">

        <!-- Table Headings search -->

        <form action="{{ url('searching') }}" method="GET" class="form-horizontal" id="search">
            @csrf
            <div class="form-group">

                <label for="studentname">
                    student name
                </label>
                <input name="filter[name]" value="{{ request()->input("filter.name") }}" id="studentname">

                <label for="studentgroup">
                    student group
                </label>
                <select name="filter[group]" value="{{ request()->input("filter.group") }}" id="studentgroup">
                    <option></option>
                    @foreach ($groups as $group)
                    @if (request()->input("filter.group") == "$group->id")
                    <option selected value="{{ $group->id }}">{{ $group->name }}</option>
                    @else
                    <option value="{{ $group->id }}">{{ $group->name }}</option>
                    @endif
                    @endforeach
                </select>

                <label for="studentaveragesince">
                    average since
                </label>
                <input type="number" placeholder="0" min="0" max="5" name="filter[av_since]" value="{{ request()->input("filter.av_since") }}"  id="studentaveragesince">

                <label for="studentaverageto">
                    to
                </label>
                <input type="number" placeholder="5" min="0" max="5" name="filter[av_to]" value="{{ request()->input("filter.av_to") }}" id="studentaverageto">
                <br>
                @foreach ($subjects as $subject)
                <label for="avg-since-{{ $subject->id }}">
                    average {{ $subject->name }} since
                </label>
                <input type="number" placeholder="since" min="0" max="5"
                       name="filterAvgSince[{{ $subject->id }}]" value="{{request()->input("filterAvgSince.$subject->id") }}" id="avg-since-{{$subject->id }}">
                <label for="avg-to-{{$subject->id }}">
                    to
                </label>
                <input type="number" placeholder="to" min="0" max="5"
                       name="filterAvgTo[{{ $subject->id }}]" value="{{ request()->input("filterAvgTo.$subject->id") }}" id="avg-to-{{$subject->id }}">
                <br>
                @endforeach

                <br>
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Search
                </button>

                @if (request('sort'))
                @foreach (request('sort') as $sortField => $value)
                <input type="hidden" name="sort[{{$sortField}}]" value="{{ $value  }}" >
                @endforeach
                @endif



            </div>
        </form>

        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
            <th>student<br>   
                <form action="{{ url('searching') }}" method="GET" class="form-horizontal" id="search2">
                    <select name="sort[student]"  value="{{ request()->input("sort.student") }}">
                        @if (request()->input("sort.student") == "asc")
                        <option></option>
                        <option selected=""  value="asc">asc</option>
                        <option value="desc">desc</option>
                        @elseif  (request()->input("sort.student") == "desc" )
                        <option></option>
                        <option  value="asc">asc</option>
                        <option selected="" value="desc">desc</option>
                        @else
                        <option></option>
                        <option  value="asc">asc</option>
                        <option  value="desc">desc</option>
                        @endif
                    </select>
                    <button type="submit">sort</button>
                    @if (request('filter'))
                    @foreach (request('filter') as $filterField => $value)
                    <input type="hidden" name="filter[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgSince'))
                    @foreach (request('filterAvgSince') as $filterField => $value)
                    <input type="hidden" name="filterAvgSince[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgTo'))
                    @foreach (request('filterAvgTo') as $filterField => $value)
                    <input type="hidden" name="filterAvgTo[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif
                </form>
            </th>
            <th>group<br>
                <form action="{{ url('searching') }}" method="GET" class="form-horizontal" id="search3">
                    <select  name="sort[group]" value="{{ request()->input("sort.group") }}">
                        @if (request()->input("sort.group") == "asc")
                        <option></option>
                        <option selected=""  value="asc">asc</option>
                        <option value="desc">desc</option>
                        @elseif  (request()->input("sort.group") == "desc")
                        <option></option>
                        <option  value="asc">asc</option>
                        <option selected="" value="desc">desc</option>
                        @else
                        <option></option>
                        <option  value="asc">asc</option>
                        <option  value="desc">desc</option>
                        @endif
                    </select>
                    <button type="submit">sort</button> 
                    @if (request('filter'))
                    @foreach (request('filter') as $filterField => $value)
                    <input type="hidden" name="filter[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgSince'))
                    @foreach (request('filterAvgSince') as $filterField => $value)
                    <input type="hidden" name="filterAvgSince[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgTo'))
                    @foreach (request('filterAvgTo') as $filterField => $value)
                    <input type="hidden" name="filterAvgTo[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif
                </form>
            </th>
            <th>average<br>
                <form action="{{ url('searching') }}" method="GET" class="form-horizontal" id="search4">
                    <select  name="sort[average]" value="{{ request()->input("sort.average") }}">
                        @if (request()->input("sort.average") == "asc")
                        <option></option>
                        <option selected=""  value="asc">asc</option>
                        <option value="desc">desc</option>
                        @elseif  (request()->input("sort.average") == "desc")
                        <option></option>
                        <option  value="asc">asc</option>
                        <option selected="" value="desc">desc</option>
                        @else
                        <option></option>
                        <option  value="asc">asc</option>
                        <option  value="desc">desc</option>
                        @endif
                    </select>
                    <button  type="submit">sort</button>  
                    @if (request('filter'))
                    @foreach (request('filter') as $filterField => $value)
                    <input type="hidden" name="filter[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgSince'))
                    @foreach (request('filterAvgSince') as $filterField => $value)
                    <input type="hidden" name="filterAvgSince[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgTo'))
                    @foreach (request('filterAvgTo') as $filterField => $value)
                    <input type="hidden" name="filterAvgTo[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif
                </form>
            </th>

            @foreach ($subjects as $subject)
            <th>average {{ $subject->name }}<br>
                <form action="{{ url('searching') }}" method="GET" class="form-horizontal">
                    <select  name="sort[avgSubject]" value="{{request()->input("sort.avgSubject")}}">
                        @if ( request()->input("sort.avgSubject") == "$subject->id"."_asc")
                        <option selected="" value="{{ $subject->id }}_asc">asc</option>
                        <option value="{{ $subject->id }}_desc">desc</option>
                        @elseif  ( request()->input("sort.avgSubject") ==  "$subject->id"."_desc")
                        <option  value="{{ $subject->id }}_asc">asc</option>
                        <option selected="" value="{{ $subject->id }}_desc">desc</option>
                        @else
                        <option></option>
                        <option  value="{{ $subject->id }}_asc">asc</option>
                        <option  value="{{ $subject->id }}_desc">desc</option>
                        @endif
                    </select>
                    <button  type="submit">sort</button>

                    @if (request('filter'))
                    @foreach (request('filter') as $filterField => $value)
                    <input type="hidden" name="filter[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgSince'))
                    @foreach (request('filterAvgSince') as $filterField => $value)
                    <input type="hidden" name="filterAvgSince[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif

                    @if (request('filterAvgTo'))
                    @foreach (request('filterAvgTo') as $filterField => $value)
                    <input type="hidden" name="filterAvgTo[{{$filterField}}]" value="{{ $value  }}" >
                    @endforeach
                    @endif
                </form>
            </th>
            @endforeach
            </thead>

            @if (count($students) > 0)
            <!-- Table Body -->
            <tbody>
                @foreach ($students as $student)
                <tr>
                    <!-- student Name -->
                    <td class="table-text">
                        <div>{{ $student->name }}</div>
                    </td>
                    <!-- group name -->

                    <td class="table-text">
                        <div>{{ $student->group["name"] }}</div>
                    </td>

                    <!-- student average -->
                    <td class="table-text">
                        <div>{{ $student->avg }}</div>
                    </td>

                    <!-- student average in subjects -->
                    @foreach ($subjects as $subject)

                    <td class="table-text">
                        <div>{{ $student->getAvgForSubject($subject) }}</div>
                    </td>

                    @endforeach
                </tr>

                @endforeach

            </tbody>
        </table>
    </div>
</div>
<div class="pagination">
    {{ $students->appends(request()->input())->links() }}
</div>
@endif

@endsection
