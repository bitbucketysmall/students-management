@extends('layouts.app')

@section('content')

@can('store', \App\Models\Student::class)
<div class="panel-body">
    <!-- Display Validation Errors -->
    @include('common.errors')
    <!-- New group Form -->
    <form action="{{ route('groups.students.store', [ $group ]) }}" method="POST" class="form-horizontal">
        {{ csrf_field() }}

        <!-- group Name -->
        <div class="form-group">
            <label for="student-name" class="col-sm-3 control-label">
                student
            </label>

            <div class="col-sm-6">
                <input type="text" name="name" id="student-name" class="form-control">
            </div>
        </div>

        <!-- Add Task Button -->
        <div class="form-student">
            <div class="col-sm-offset-3 col-sm-6">
                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Add student
                </button>
            </div>
        </div>
    </form>
</div>
@endcan

<div class="panel panel-default">
    <div class="panel-heading"></div>
    <div class="panel-body">
        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
            <th>name</th>
            <th>birth</th>
            </thead>
            @if (count($studentsInGroup) > 0)
            <!-- Table Body -->
            <tbody>
                @foreach ($studentsInGroup as $student)
                <tr>
                    <!-- student Name -->
                    <td class="table-text">
                        <div>{{ $student->name }}</div>
                    </td>
                    <!-- student birth -->
                    <td class="table-text">
                        <div>{{ $student->birth }}</div>
                    </td>
                    @can('destroy', $student)
                    <td>
                        <!--  Delete Button -->
                        <form action="{{ route('groups.students.destroy', [ $student->group, $student]) }}" method="POST">
                            {{ csrf_field() }}
                            {{ method_field('DELETE') }}

                            <button type="submit" id="delete-group-{{ $student->id }}" class="btn btn-danger">
                                <i class="fa fa-btn fa-trash"></i>Delete
                            </button>
                        </form>
                    </td>
                    @endcan                
                    <!-- student bio -->
                    <td class="table-text">
                        <form id="update{{ $student->name }}"
                              action="{{ route('groups.students.show', [ $student->group, $student]) }}" method="GET">
                            {{ csrf_field() }}

                            <button type="submit" id="students-personal-{{ $student->id }}" class="btn btn-light">
                                <i class="fa fa-btn fa-trash"></i>see a bio
                            </button>
                        </form>
                    </td>
                </tr>
                @endforeach

            </tbody>
            @endif
        </table>
    </div>
</div>

@endsection

