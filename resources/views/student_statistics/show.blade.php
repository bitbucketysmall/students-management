@extends('layouts.app')

@section('content')
<div class="panel panel-default">
    <div class="panel-body">      
        <div class="panel-heading">
            Досье:
        </div>
        <img src="{{ $student->profilePhoto }}">
        <div>Имя: {{ $student->name }}</div>
        <div>Рождение: {{ $student->birth }}</div>
        <div>Группа: {{ $student->group->name }}</div>
    </div>
</div>

<div class="panel panel-default">
    <div class="panel-heading">
        Ratings
    </div>

    <form class="form-horizontal" action="{{url('groups/'.$student->group->id.'/students/'. $student->id .'/marks') }}" enctype="multipart/form-data" method="POST">
        {{ csrf_field() }}
        <div class="form-group">
            <div class="col-sm-6">
                <select name="subject_id" required="">
                    <option value="">subject</option>
                    @foreach ($subjects as  $subj)
                    <option  value="{{$subj->id}}">{{$subj->name}}</option>
                    @endforeach
                </select>
                <select name="mark" required="">
                    <option value="">mark</option>
                    <option  value="1">1</option>
                    <option  value="2">2</option>
                    <option  value="3">3</option>
                    <option  value="4">4</option>
                    <option  value="5">5</option>
                </select>
                <input type="submit" value="New mark">
            </div>
        </div>
    </form>

    <div class="panel-body user_subject_chart">
        @foreach ($student->grouppedMarks as $subject => $marks)
        <div class="user_mark_chart">
            <span>{{ $subject }}</span>
            @foreach ($marks as  $mark)
            <div class="user_inside_mark_chart">
                <span>{{ $mark->mark }}</span>
                <!--  Delete Button -->
                <form action="{{url('groups/'.$student->group->id.'/students/'. $student->id .'/marks/' .$mark->id) }}
                      " method="POST">
                    {{ csrf_field() }}
                    {{ method_field('DELETE') }}
                    <button class="user_inside_mark_button" type="submit" id="delete-studentsubjet-{{ $mark->id }}" >
                        Delete
                    </button>
                </form>

            </div>
            @endforeach
        </div>
        @endforeach            
    </div>
    
    <br>
    @can('edit', \App\Models\Student::class)
     <div class="panel-body user_subject_chart">      
        <div>      
            <form class="form-horizontal"
                  action="{{url('groups/'.$student->group->id.'/students/'. $student->id .'/edit' ) }}" method="GET">
                <div class="form-group">
                    <div class="col-sm-6">                 
                        <button type="submit" id="students-personal-{{ $student->id }}" class="btn btn-info">
                            <i class="fa fa-btn fa-trash"></i>Update bio
                        </button>
                    </div>
                </div>
            </form>
        </div>
    </div>
    @endcan

</div>



@endsection

