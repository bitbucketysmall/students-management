@extends('layouts.app')
@section('content')

<!-- Current students -->

<div class="panel panel-default">
    <div class="panel-heading">
        SEARCH PREFERENCIES
    </div>

    <div class="panel-body">

        <!-- Table Headings search -->

        <form action="{{ url('searching') }}" method="GET" class="form-horizontal" id="search">
            @csrf
            <div class="form-group">

                <label for="studentname">
                    student name
                </label>
                <input placeholder="name" name="name" value="{{ request('name') }}" id="studentname">

                <label for="studentgroup">
                    student group
                </label>
                <select name="group" id="studentgroup">
                    <option></option>
                    @foreach ($groups as $group)
                        @if (request('group') == $group->id)
                            <option selected value="{{ $group->id }}">{{ $group->name }}</option>
                        @else
                            <option value="{{ $group->id }}">{{ $group->name }}</option>
                        @endif
                    @endforeach
                </select>

                <label for="studentaveragesince">
                    average since
                </label>
                <input type="number" placeholder="0" min="0" max="5" name="av_since" value="{{ request('av_since') }}" id="studentaveragesince">

                <label for="studentaverageto">
                     to
                </label>
                <input type="number" placeholder="5" min="0" max="5" name="av_to" value="{{ request('av_to') }}" id="studentaverageto">

                <br>



                @foreach ($subjects as $subject)
                <label for="avg-since-{{ $subject->id }}">
                    average {{ $subject->name }} since
                </label>
                <input type="number" placeholder="since" min="0" max="5"
                       name="avg_since[{{ $subject->id }}]" value="{{ request()->input("avg_since.{$subject->id}") }}" id="avg-since-{{ $subject->id }}">
                <label for="avg-to-{{ $subject->id }}">
                  to
                </label>
                <input type="number" placeholder="to" min="0" max="5"
                       name="avg_to[{{ $subject->id }}]" value="{{ request()->input("avg_to.{$subject->id}") }}" id="avg-to-{{ $subject->id }}">
                <br>
                @endforeach

                <button type="submit" class="btn btn-default">
                    <i class="fa fa-plus"></i> Search
                </button>
            </div>
        </form>




        <table class="table table-striped task-table">

            <!-- Table Headings -->
            <thead>
            <th>student<br>
                <input form="search" type="submit" name="sortstudent" value="asc">

                <button form="search" type="submit" name="sortstudent" value="desc">desc</button>
            </th>
            <th>group<br>
                <button form="search" type="submit" name="sortgroup" value="asc">asc</button>
                <button form="search" type="submit" name="sortgroup" value="desc">desc</button>
            </th>
            <th>average<br>
                <button form="search" type="submit" name="sortaverage" value="asc">asc</button>
                <button form="search" type="submit" name="sortaverage" value="desc">desc</button>
            </th>




            @foreach ($subjects as $v)
            <th>average {{ $v->name }}<br>
                <button form="search" type="submit" name="sortaverageSubject" value="asc_{{ $v->id }}">asc</button>
                <button form="search" type="submit" name="sortaverageSubject" value="desc_{{ $v->id }}">desc</button>
            </th>

            @endforeach
           
            </thead>



            @if (count($students) > 0)
            <!-- Table Body -->
            <tbody>
                @foreach ($students as $student)
                <tr>
                    <!-- student Name -->
                    <td class="table-text">
                        <div>{{ $student->name }}</div>
                    </td>
                    <!-- group name -->

                    <td class="table-text">
                        <div>{{ $student->group["name"] }}</div>
                    </td>

                    <!-- student average -->
                    <td class="table-text">
                        <div>{{ $student->avg }}</div>
                    </td>

                    <!-- student average in subjects -->
                    @foreach ($subjects as $subject)

                    <td class="table-text">
                        <div>{{ $student->getAvgForSubject($subject) }}</div>
                    </td>

                    @endforeach
                </tr>

                @endforeach

            </tbody>
        </table>
    </div>
    
</div>
@endif




@endsection
