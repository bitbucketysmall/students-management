<?php

namespace App\Http\Controllers;

use App\Models\Subject;
use Illuminate\Http\Request;
use App\Http\Requests\SubjectForm;


class SubjectController extends Controller {

    public function index()
    {    
        $subjects = Subject::orderBy('name')->get();
        
        return view('subject.index', [
            'subjects' => $subjects,         
        ]);
    }


    public function store(SubjectForm $request) {

        Subject::create($request->all());

        return redirect('/subjects');
    }

    public function show(Subject $subject) {
        
        return view('subject.show', [
            'subject' => $subject,
        ]);
    }

    public function update(SubjectForm $request, Subject $subject) {
        
        $subject->update($request->all());
        
        return redirect('/subjects');
    }

    public function destroy(Subject $subject) {
        $subject->delete();
        
        return redirect('/subjects');
    }

}
