<?php

namespace App\Http\Controllers;

use App\Models\Group;
use App\Models\Subject;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Requests\GroupForm;
use App\CollectionAvg\CollectionAvg;

class GroupController extends Controller {

    public function index(CollectionAvg $collectionAvg) {
        $groups = Group::with('marks.subject')->get();
        $subjects = Subject::orderBy('name')->get();
        
         $collectionAvg->avgForCollection($groups);
         $collectionAvg->avgInSubjForCollection($groups, $subjects);
        
        return view('group_statistics.index', [
            'groups' => $groups,
            'subjects' => $subjects,
        ]);
    }

    public function store(GroupForm $request) {
        
        Group::create($request->all());

        return redirect('/groups');
    }

    public function show(Group $group) {
        
        return view('group_statistics.show', [
            'group' => $group,
        ]);
    }

    public function update(GroupForm $request, Group $group) {
        
        $group->update($request->all());
        
        return redirect('/groups');
    }

    public function destroy(Group $group) {
        
        $group->delete();
        
        return redirect('/groups');
    }

}
