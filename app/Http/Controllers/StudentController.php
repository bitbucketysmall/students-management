<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;
use App\Models\Mark;
use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Requests\StudentFormUpdate;
use App\Http\Requests\StudentFormStore;
use App\Http\Requests\StudentUploadPhoto;
use App\CollectionAvg\CollectionAvg;
use \App\Filters\StudentFilter;
use Illuminate\Support\Facades\Storage;

class StudentController extends Controller {

    public function index(Group $group) {

        $studentsInGroup = Student::where('group_id', '=', $group->id)->get();
        return view('student_statistics.index', [
            'studentsInGroup' => $studentsInGroup,
            'group' => $group,
        ]);
    }

    public function store(StudentFormStore $request) {

        Student::create($request->all());

        return redirect()->back();
    }

    public function search(Request $request, StudentFilter $filter, CollectionAvg $collectionAvg) {
        
        $subjects = Subject::orderBy('name', 'asc')->get();
        $groups = Group::all();
        $query = Student::with('marks.subject', 'group');


        print_r($request->input());


        if ($request->input()) {
            $filter->applyFiltersToQuery($query, $request);
        }
        $students = $query->paginate();
        //$students = $query->get();

        return view('student_statistics.search', [
            'students' => $students,
            'subjects' => $subjects,
            'groups' => $groups,
        ]);
    }

    public function destroy(Group $group, Student $student) {

        if ('default.png' != $student->image) {
            Storage::disk('public')->delete('images/' . $student->image);
        }
        $student->delete();

        return redirect()->back();
    }

    public function uploadPhoto(Request $request, Group $group, Student $student) {

        if ('default.png' != $student->image) {
            Storage::disk('public')->delete('images/' . $student->image);
        }
        $file = $request->file('image');
        $filePath = $request->file('image')->getRealPath();
        $fileName = $student->id . '.' .$file->getClientOriginalExtension();      
        $image_resize = Image::make($filePath)->resize(200, 200);
        Storage::disk('public')->put('images/'.$fileName, $image_resize->encode());

        $student->image = $fileName;
        $student->save();
        return redirect()->back();
    }

    public function update(StudentFormUpdate $request, Group $group, Student $student) {

        $student->update($request->all());

        return redirect()->back();
    }

    public function edit(Group $group, Student $student) {

        $groups = Group::orderBy('name')->get();

        return view('student_statistics.edit', [
            'currentStudent' => $student,
            'groups' => $groups,
        ]);
    }

    public function show(Request $request, Group $group, Student $student) {

        $subjects = Subject::orderBy('name')->get();
        $groups = Group::orderBy('name')->get();
        $student = Student::with('marks.subject', 'group')->find($student->id);
        
        return view('student_statistics.show', [
            'student' => $student,
            'subjects' => $subjects,
            'groups' => $groups,
        ]);
    }

}
