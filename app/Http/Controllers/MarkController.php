<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Mark;
use App\Models\Group;
use App\Models\Student;
use App\Models\Subject;


class MarkController extends Controller {

    public function store(Request $request, Group $group, Student $student) {

        $subject = Subject::find($request->input('subject_id'));
        $mark = new Mark(['mark' => $request->input('mark')]);
        $mark->subject()->associate($subject);
        $mark->student()->associate($student);
        $mark->save();
        
        return redirect()->back();
    }

    public function destroy(Group $group, Student $student, Mark $mark) {
        $mark->delete();

        return redirect()->back();
    }

}