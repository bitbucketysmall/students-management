<?php

namespace App\CollectionAvg;
use App\Models\Subject;

class CollectionAvg {

    public function avgForCollection($entities) {
        foreach ($entities as $entity) {
            $entity['average'] = $entity->marks->avg('mark') ?: 0;
        }
    }

    public function avgInSubjForCollection($entities, $subjects ) {

        // group subject marks together by subject name
        foreach ($entities as $entity) {
            $grouped_subjects_in_students[] = $entity->marks->groupBy('subject.name');
            unset($entity->marks);
            $entity->marks = collect(last($grouped_subjects_in_students)->all())->sortKeys();
        }
        // let`s compute avg by subject_id
        foreach ($entities as $entity) {
            $avgInSubjects = [];
            foreach ($subjects as $subj) {
                $avgInSubjects [$subj->name] = 0;
            }
            foreach ($entity->marks as $k => $subjectGroup) {
                $avgForSubject = $subjectGroup->avg('mark');
                $avgInSubjects[$k] = $avgForSubject;
            }
            $entity['averageSubjects'] = $avgInSubjects;
        }
    }

}
