<?php

namespace App\Filters;

use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\Builder;

class StudentFilter {

    protected $query;
    protected $request;

    public function applyFiltersToQuery(Builder $query, Request $request) {


        if ($request->filled('filter.name')) {
            $this->applyFilterForName($query, $request);
        }
        if ($request->filled('filter.av_since')) {
            $this->applyFilterForAvg($query, $request);
        }
        if ($request->filled('filter.group')) {
            $this->applyFilterForGroup($query, $request);
        }
        if ($request->filled('filterAvgSince')) {
            $this->applyFilterForAvgInSubjects($query, $request);
        }
        
        // do sorting
        if ($request->filled('sort.student')) {
            $this->applySortingForName($query, $request);
        }
        if ($request->filled('sort.group')) {
            $this->applySortingForGroup($query, $request);
        }
        if ($request->filled('sort.average')) {
            $this->applySortingForAvg($query, $request);
        }
        if ($request->filled('sort.avgSubject')) {
            $this->applySortingForAvgInSubjects($query, $request);
        }
    }

    private function applyFilterForAvgInSubjects(Builder $query, Request $request) {
        foreach ($request->input("filterAvgSince", []) as $subjectId => $avg) {
            if (!$avg) {
                continue;
            }
            $query->whereHas('marks', function ($q) use ($avg, $subjectId, $request) {
                $q->select('marks.subject_id')
                        ->selectRaw('avg(mark) as avgInSubject');
                $q->where('marks.subject_id', '=', $subjectId);
                $q->groupBy('student_id');
                $q->havingRaw('avg(mark) > ?', [$avg]);
                if ($request->has("filterAvgTo.{$subjectId}")) {
                    $q->havingRaw('avg(mark) < ?', [$request->input("filterAvgTo.{$subjectId}")]);
                }
            });
        }
    }

    private function applyFilterForAvg(Builder $query, Request $request) {
        $query->whereHas('marks', function ($q) use ($request) {
            $q->selectRaw('avg(mark)');
            $q->groupBy('student_id');
            $limitLeft = $request->input('filter.av_since');
            $q->havingRaw('avg(mark) > ?', [$limitLeft]);
            if ($request->has('filter.av_to')) {
                $limitRight = $request->input('filter.av_to');
                $q->havingRaw('avg(mark) < ?', [$limitRight]);
            }
        });
    }

    private function applyFilterForName(Builder $query, Request $request) {
        $query->where('name', 'like', $request->input('filter.name') . '%');
    }

    private function applyFilterForGroup(Builder $query, Request $request) {
        $query->where('group_id', $request->input('filter.group'));
    }

    private function applySortingForName(Builder $query, Request $request) {
        $query->orderBy('students.name', $request->input('sort.student'));
    }

    private function applySortingForGroup(Builder $query, Request $request) {
        $order = $request->input('sort.group');
        $query->join('groups', 'students.group_id', '=', 'groups.id');
        $query->select('groups.name', 'students.*');
        $query->orderBy('groups.name', $order);
    }

    private function applySortingForAvg(Builder $query, Request $request) {
        $order = $request->input('sort.average');
        $query->join('marks', 'students.id', '=', 'marks.student_id');
        $query->select('students.*', \DB::raw('avg(marks.mark) as average'));
        $query->groupBy('students.id');
        $query->orderBy('average', $order);
        $this->alreadyJoinedMarks = true;
    }

    private function applySortingForAvgInSubjects(Builder $query, Request $request) {

        $params = explode('_', $request->input("sort.avgSubject"));
        $query->join('marks', 'students.id', '=', 'marks.student_id');
        $query->select('students.*', 'marks.subject_id', \DB::raw('avg(marks.mark) as average'));
        $query->where('marks.subject_id', '=', $params[0]);
        $query->groupBy('students.id');
        $query->orderBy('average', $params[1]);
    }

}
