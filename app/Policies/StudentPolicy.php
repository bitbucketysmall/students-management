<?php

namespace App\Policies;

use App\Models\User;
use Illuminate\Auth\Access\HandlesAuthorization;

class StudentPolicy
{
    use HandlesAuthorization;

    public function store(User $user)
    {
        return $user->isAdmin();
    }

    public function update(User $user)
    {
        return $user->isAdmin();
    }
    
    public function edit(User $user)
    {
        return $user->isAdmin();
    }

    public function destroy(User $user)
    {
        return $user->isAdmin();
    }
}
