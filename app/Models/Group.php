<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $fillable = ['name'];

    public function students()
    {
        return $this->hasMany(Student::class, 'group_id');
    }

    public function marks()
    {
        return $this->hasManyThrough(Mark::class, Student::class, 'group_id', 'student_id');
    }
}
