<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Storage;

class Student extends Model {

    protected $table = 'students';
    protected $fillable = ['name', 'birth', 'group_id', 'image'];

    public function group() {
        return $this->belongsTo(Group::class, 'group_id');
    }

    public function marks() {
        return $this->hasMany(Mark::class);
    }

    public function getGrouppedMarksAttribute() {
        return $this->marks->groupBy('subject.name')->sortKeys();
    }

    public function getAvgAttribute() {
        return  $this->marks->avg('mark') ?: 0;
    }
    
    public function getAvgForSubject(Subject $subject) {
        
        return $this->marks->where('subject_id', '=', $subject->id)->avg('mark');

    }
    
    public function getProfilePhotoAttribute() {
        
        //return  asset('storage/images/'.$this->image);
        return Storage::url('images/'.$this->image);
    }

}
