<?php

Route::view('/', 'welcome');
Route::auth();

Route::get('/search', 'StudentController@search');
Route::get('/searching', 'StudentController@search');

Route::post('groups/{group}/students/{student}/uploadPhoto', 'StudentController@uploadPhoto');

Route::resource('groups.students', 'StudentController');
Route::resource('groups.students.marks', 'MarkController');

Route::resources([
    'subjects' => 'SubjectController',
    'groups' => 'GroupController',
]);


