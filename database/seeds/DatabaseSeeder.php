<?php

use Illuminate\Database\Seeder;

use App\Models\Mark;
use App\Models\Group;
use App\Models\Subject;
use App\Models\Student;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $subjects = factory(Subject::class, 3)->create();

        $groups = factory(Group::class, 3)->create()
        ->each(function ($group) use ($subjects) {
            $students = factory(Student::class, 8)->make()
            ->each(function ($student) use ($subjects, $group) {
                $group->students()->save($student);
                foreach ($subjects as $subject) {
                    factory(Mark::class, 2)->make()
                    ->each(function ($mark) use ($student, $subject) {
                        $mark->subject()->associate($subject);
                        $student->marks()->save($mark);
                    });
                }
            });
        });
    }
}
