<?php

use Faker\Generator as Faker;

$factory->define(App\Models\Student::class, function (Faker $faker) {
    return [
        'name' => $faker->name,
        'birth' => $faker->dateTime,
    ];
});

